package com.kowsalya.attendance.utils;


import android.app.Application;
import android.text.TextUtils;

public class AppController extends Application {

    @Override
    public void onCreate() {
        super.onCreate();

        ApplicationSettings.init(getApplicationContext());
    }
}
