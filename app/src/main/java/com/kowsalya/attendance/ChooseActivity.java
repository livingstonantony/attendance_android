package com.kowsalya.attendance;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

import com.bosco.attendance.R;
import com.kowsalya.attendance.ui.activities.StaffLoginActivity;
import com.kowsalya.attendance.ui.activities.StudentLoginActivity;

public class ChooseActivity extends AppCompatActivity {

    private Button mStudent;
    private Button mStaff;

    static final int REQUEST_TAKE_PHOTO = 1;
    static final int REQUEST_GALLERY_PHOTO = 2;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_choose);

        mStudent = findViewById(R.id.buttonStudent);
        mStaff = findViewById(R.id.buttonStaff);



        mStudent.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(ChooseActivity.this, StudentLoginActivity.class);
                startActivity(intent);
            }
        });

        mStaff.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(ChooseActivity.this, StaffLoginActivity.class);
                startActivity(intent);
            }
        });

    }


}
