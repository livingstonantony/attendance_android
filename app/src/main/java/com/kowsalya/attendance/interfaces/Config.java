package com.kowsalya.attendance.interfaces;

public interface Config {
    String PROFILE = "profile";
    String ID = "id";
    String NAME = "name";
    String PASSWORD = "password";
    String SUBJECT = "subject";

    String STUDENT_PROFILE = "studentProfile";
    String STUDENT_ID = "studentId";
    String STUDENT_NAME = "studentName";
    String STUDENT_PASSWORD = "studentPassword";
    String STUDENT_COURSE = "studentCourse";
    String STUDENT_GENDER = "studentGender";
}
