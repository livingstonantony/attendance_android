package com.kowsalya.attendance.ui.student_login;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class StudentLoginResponse {

    @SerializedName("response")
    @Expose
    private ProfileData profileData;

    public StudentLoginResponse(ProfileData profileData) {
        this.profileData = profileData;
    }

    public ProfileData getProfileData() {
        return profileData;
    }

    public void setProfileData(ProfileData profileData) {
        this.profileData = profileData;
    }

    public class ProfileData {

        @SerializedName("profile")
        @Expose
        private String profile;

        @SerializedName("id")
        @Expose
        private String id;

        @SerializedName("name")
        @Expose
        private String name;

        @SerializedName("password")
        @Expose
        private String password;

        @SerializedName("course")
        @Expose
        private String subject;

        @SerializedName("gender")
        @Expose
        private int gender;


        public ProfileData(String profile, String id, String name, String password, String subject, int gender) {
            this.profile = profile;
            this.id = id;
            this.name = name;
            this.password = password;
            this.subject = subject;
            this.gender = gender;
        }

        public String getProfile() {
            return profile;
        }

        public void setProfile(String profile) {
            this.profile = profile;
        }

        public String getId() {
            return id;
        }

        public void setId(String id) {
            this.id = id;
        }

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }

        public String getPassword() {
            return password;
        }

        public void setPassword(String password) {
            this.password = password;
        }

        public String getSubject() {
            return subject;
        }

        public void setSubject(String subject) {
            this.subject = subject;
        }

        public int getGender() {
            return gender;
        }

        public void setGender(int gender) {
            this.gender = gender;
        }
    }
}
