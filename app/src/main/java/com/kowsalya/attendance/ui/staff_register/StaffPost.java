package com.kowsalya.attendance.ui.staff_register;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class StaffPost {

    @SerializedName("staff_profile")
    @Expose
    private String staffProfile;

    @SerializedName("staff_id")
    @Expose
    private String staffId;

    @SerializedName("staff_name")
    @Expose
    private String staffName;

    @SerializedName("staff_password")
    @Expose
    private String staffPassword;

    @SerializedName("staff_subject")
    @Expose
    private String staffSubject;

    public StaffPost() {
    }

    public StaffPost(String staffProfile, String staffId, String staffName, String staffPassword, String staffSubject) {
        this.staffProfile = staffProfile;
        this.staffId = staffId;
        this.staffName = staffName;
        this.staffPassword = staffPassword;
        this.staffSubject = staffSubject;
    }

    public String getStaffProfile() {
        return staffProfile;
    }

    public void setStaffProfile(String staffProfile) {
        this.staffProfile = staffProfile;
    }

    public String getStaffId() {
        return staffId;
    }

    public void setStaffId(String staffId) {
        this.staffId = staffId;
    }

    public String getStaffName() {
        return staffName;
    }

    public void setStaffName(String staffName) {
        this.staffName = staffName;
    }

    public String getStaffSubject() {
        return staffSubject;
    }

    public void setStaffSubject(String staffSubject) {
        this.staffSubject = staffSubject;
    }

    public String getStaffPassword() {
        return staffPassword;
    }

    public void setStaffPassword(String staffPassword) {
        this.staffPassword = staffPassword;
    }
}
