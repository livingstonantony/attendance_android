package com.kowsalya.attendance.ui.add_student;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class ResponseFromAPI {
    @SerializedName("response")
    @Expose
    private String response;

    public ResponseFromAPI(String response) {
        this.response = response;
    }

    public String getResponse() {
        return response;
    }

    public void setResponse(String response) {
        this.response = response;
    }
}
