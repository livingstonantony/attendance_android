package com.kowsalya.attendance.ui.students;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Student {

    @SerializedName("id")
    @Expose
    private int id;

    @SerializedName("student_id")
    @Expose
    private String studentId;

    @SerializedName("student_name")
    @Expose
    private String studentName;

    @SerializedName("student_attendance")
    @Expose
    private int studentAttendance;

    @SerializedName("student_time")
    @Expose
    private long studentTime;

    @SerializedName("student_img_url")
    @Expose
    private String studentImgUrl;

    @SerializedName("course")
    @Expose
    private String course;

    @SerializedName("gender")
    @Expose
    private int gender;

    public Student(int id, String studentId, String studentName, int studentAttendance, long studentTime, String studentImgUrl, String course, int gender) {
        this.id = id;
        this.studentId = studentId;
        this.studentName = studentName;
        this.studentAttendance = studentAttendance;
        this.studentTime = studentTime;
        this.studentImgUrl = studentImgUrl;
        this.course = course;
        this.gender = gender;
    }

    public Student(int id, String studentId, String studentName, int studentAttendance, long studentTime, String studentImgUrl) {
        this.id = id;
        this.studentId = studentId;
        this.studentName = studentName;
        this.studentAttendance = studentAttendance;
        this.studentTime = studentTime;
        this.studentImgUrl = studentImgUrl;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getStudentId() {
        return studentId;
    }

    public void setStudentId(String studentId) {
        this.studentId = studentId;
    }

    public String getStudentName() {
        return studentName;
    }

    public void setStudentName(String studentName) {
        this.studentName = studentName;
    }

    public int getStudentAttendance() {
        return studentAttendance;
    }

    public void setStudentAttendance(int studentAttendance) {
        this.studentAttendance = studentAttendance;
    }

    public long getStudentTime() {
        return studentTime;
    }

    public void setStudentTime(long studentTime) {
        this.studentTime = studentTime;
    }

    public String getStudentImgUrl() {
        return studentImgUrl;
    }

    public void setStudentImgUrl(String studentImgUrl) {
        this.studentImgUrl = studentImgUrl;
    }

    public String getCourse() {
        return course;
    }

    public void setCourse(String course) {
        this.course = course;
    }

    public int getGender() {
        return gender;
    }

    public void setGender(int gender) {
        this.gender = gender;
    }
}
