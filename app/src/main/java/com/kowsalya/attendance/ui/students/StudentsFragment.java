package com.kowsalya.attendance.ui.students;

import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AlertDialog;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProvider;
import androidx.recyclerview.widget.DefaultItemAnimator;
import androidx.recyclerview.widget.DividerItemDecoration;
import androidx.recyclerview.widget.RecyclerView;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;

import com.bosco.attendance.R;
import com.kowsalya.attendance.interfaces.Config;
import com.kowsalya.attendance.utils.ApplicationSettings;
import com.kowsalya.attendance.utils.Utils;

import java.util.ArrayList;
import java.util.List;

public class StudentsFragment extends Fragment {

    private StudentsViewModel attendanceViewModel;

    private List<Student> list;
    private StudentAdapter mAdapter;
    private RecyclerView mRecyclerView;

    private SwipeRefreshLayout mSwipeToRefresh;

    private TextView mTextViwNoData;

    AlertDialog dialog;
    public View onCreateView(@NonNull LayoutInflater inflater,
                             ViewGroup container, Bundle savedInstanceState) {


        View root = inflater.inflate(R.layout.fragment_students, container, false);


        return root;
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

         dialog = Utils.showDialog(getActivity());

        attendanceViewModel = new ViewModelProvider(this).get(StudentsViewModel.class
        );

        mTextViwNoData = view.findViewById(R.id.textViewNoData);

        list = new ArrayList<>();

        mSwipeToRefresh = view.findViewById(R.id.swipeTorRefreshStudents);

        mRecyclerView = view.findViewById(R.id.recyclerViewAttendance);
        mAdapter = new StudentAdapter(list);
        mRecyclerView.addItemDecoration(new DividerItemDecoration(getActivity(), DividerItemDecoration.VERTICAL));
        mRecyclerView.setItemAnimator(new DefaultItemAnimator());
        mRecyclerView.setAdapter(mAdapter);



        attendanceViewModel.init(ApplicationSettings.read(Config.ID,""));


        mSwipeToRefresh.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                attendanceViewModel.init(ApplicationSettings.read(Config.ID,""));

                getStudents();
            }
        });

        getStudents();
    }

    private void getStudents() {

        dialog.show();

        attendanceViewModel.getNewsRepository().observe(getViewLifecycleOwner(), new Observer<StudentsResponse>() {
            @Override
            public void onChanged(StudentsResponse studentsResponse) {

                dialog.dismiss();
                mSwipeToRefresh.setRefreshing(false);

                if (studentsResponse != null) {
                    if (studentsResponse.getList().size() != 0) {
                        mTextViwNoData.setVisibility(View.GONE);
                        list.clear();
                        list.addAll(studentsResponse.getList());
                        mAdapter.notifyDataSetChanged();
                    }
                }
                Log.d("RESPONSE", " : " + studentsResponse);
            }
        });

    }
}
