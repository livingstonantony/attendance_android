package com.kowsalya.attendance.ui.mark_attendance;


import com.kowsalya.attendance.ui.add_student.ResponseFromAPI;

import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.POST;

public interface AttendanceApi {
    @POST("attendance")
    Call<ResponseFromAPI> setAttendance(@Body AttendanceList list);
}
