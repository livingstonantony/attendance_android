package com.kowsalya.attendance.ui.student_change_password;


import com.kowsalya.attendance.ui.add_student.ResponseFromAPI;
import com.kowsalya.attendance.ui.satff_change_password.ChangePasswordBean;

import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.GET;
import retrofit2.http.POST;
import retrofit2.http.Query;

public interface StudentChangePasswordApi {
    @POST("change_student_password")
    Call<ResponseFromAPI> setPassword(@Body ChangePasswordBean bean);

    @GET("delete_student")
    Call<ResponseFromAPI> deleteAccount(@Query("id") String id);
}
