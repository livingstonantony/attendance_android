package com.kowsalya.attendance.ui.satff_change_password;


import androidx.lifecycle.MutableLiveData;

import com.kowsalya.attendance.retrofit.RetrofitService;
import com.kowsalya.attendance.ui.add_student.ResponseFromAPI;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class StaffChangePasswordRepository {

    private static StaffChangePasswordRepository registerRepository;

    public static StaffChangePasswordRepository getInstance() {
        if (registerRepository == null) {
            registerRepository = new StaffChangePasswordRepository();
        }
        return registerRepository;
    }

    private StaffChangePasswordApi api;

    public StaffChangePasswordRepository() {
        api = RetrofitService.createService(StaffChangePasswordApi.class);
    }


    public MutableLiveData<ResponseFromAPI> setPassword(ChangePasswordBean bean) {
        final MutableLiveData<ResponseFromAPI> mutableLiveData = new MutableLiveData<>();

        api.setPassword(bean).enqueue(new Callback<ResponseFromAPI>() {
            @Override
            public void onResponse(Call<ResponseFromAPI> call, Response<ResponseFromAPI> response) {
                if (response.isSuccessful()) {

                    mutableLiveData.setValue(response.body());
//                    mutableLiveData.setValue(null);
                }
            }

            @Override
            public void onFailure(Call<ResponseFromAPI> call, Throwable t) {
                mutableLiveData.setValue(null);
            }
        });

        return mutableLiveData;
    }
}
