package com.kowsalya.attendance.ui.student_login;


import com.kowsalya.attendance.ui.staff_login.StaffLoginResponse;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Query;

public interface StudentLoginApi {
    @GET("login_student")
    Call<StudentLoginResponse> getStudentProfile(@Query("id") String id, @Query("password") String password);
}
