package com.kowsalya.attendance.ui.student_login;

import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.ViewModel;

import com.kowsalya.attendance.ui.staff_login.StaffLoginRepository;
import com.kowsalya.attendance.ui.staff_login.StaffLoginResponse;

public class StudentLoginViewModel extends ViewModel {

    private MutableLiveData<String> mText;
    private MutableLiveData<StudentLoginResponse> mutableLiveData;
    private StudentLoginRepository registerRepository;

    public void init(String id, String password) {
       /* if (mutableLiveData != null){
            return;
        }*/
        registerRepository = StudentLoginRepository.getInstance();

        mutableLiveData = registerRepository.getStudentProfile(id, password);

    }

    public StudentLoginViewModel() {
        mText = new MutableLiveData<>();
        mText.setValue("This is attendance fragment");
    }


    public LiveData<StudentLoginResponse> getRegisterResponse() {
        return mutableLiveData;
    }
}