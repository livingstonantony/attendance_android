package com.kowsalya.attendance.ui.staff_profile;

import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProvider;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Toast;

import com.bosco.attendance.R;
import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;
import com.kowsalya.attendance.interfaces.Config;
import com.kowsalya.attendance.ui.add_student.ResponseFromAPI;
import com.kowsalya.attendance.ui.student.StudentUpdateProfileViewModel;
import com.kowsalya.attendance.utils.AppController;
import com.kowsalya.attendance.utils.ApplicationSettings;

/**
 * A simple {@link Fragment} subclass.
 */
public class StaffProfileFragment extends Fragment {

    private EditText mEditTextId;
    private EditText mEditTextName;
    private EditText mEditTextSubject;

    private ImageView mImageViewProfile;

    private Button mButtonUpdate;

    private StaffUpdateProfileViewModel viewModel;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_staff_profile, container, false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        viewModel = new ViewModelProvider(this).get(StaffUpdateProfileViewModel.class);


        mButtonUpdate = view.findViewById(R.id.buttonUpdate);

        mImageViewProfile = view.findViewById(R.id.imageViewProfile);

        mEditTextId = view.findViewById(R.id.editTextId);
        mEditTextName = view.findViewById(R.id.editTextName);
        mEditTextSubject = view.findViewById(R.id.editTextSubject);

        mEditTextId.setText(ApplicationSettings.read(Config.ID, ""));
        mEditTextName.setText(ApplicationSettings.read(Config.NAME, ""));
        mEditTextSubject.setText(ApplicationSettings.read(Config.SUBJECT, ""));

        Glide.with(getContext()).load(ApplicationSettings.read(Config.PROFILE, "")).apply(new RequestOptions().placeholder(R.drawable.person)).into(mImageViewProfile);


        mButtonUpdate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String id = mEditTextId.getText().toString().trim();
                String name = mEditTextName.getText().toString().trim();
                String subject = mEditTextSubject.getText().toString().trim();

                StaffDetailsBean bean = new StaffDetailsBean(id, name, subject);

                viewModel.init(bean);
                viewModel.getRegisterResponse().observe(getViewLifecycleOwner(), new Observer<ResponseFromAPI>() {
                    @Override
                    public void onChanged(ResponseFromAPI registerResponse) {
                        Toast.makeText(getContext(), "" + registerResponse, Toast.LENGTH_SHORT).show();
                    }
                });

            }
        });
    }
}
