package com.kowsalya.attendance.ui.attendance;


import androidx.lifecycle.MutableLiveData;

import com.kowsalya.attendance.retrofit.RetrofitService;
import com.kowsalya.attendance.ui.students.StudentsApi;
import com.kowsalya.attendance.ui.students.StudentsResponse;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class AttendancePDFRepository {

    private static AttendancePDFRepository newsRepository;

    public static AttendancePDFRepository getInstance(){
        if (newsRepository == null){
            newsRepository = new AttendancePDFRepository();
        }
        return newsRepository;
    }

    private AttendancePDFApi api;

    public AttendancePDFRepository(){
        api = RetrofitService.createService(AttendancePDFApi.class);
    }

    public MutableLiveData<AttendanceBean> getAllPDF(String id,String name,String subject){

        final MutableLiveData<AttendanceBean> newsData = new MutableLiveData<>();

            api.getAllPDF(id,name,subject).enqueue(new Callback<AttendanceBean>() {
                @Override
                public void onResponse(Call<AttendanceBean> call, Response<AttendanceBean> response) {
                    if (response.isSuccessful()){
                        newsData.setValue(response.body());
                    }
                }

                @Override
                public void onFailure(Call<AttendanceBean> call, Throwable t) {
                    newsData.setValue(null);
                }
            });


        return newsData;
    }

    public MutableLiveData<AttendanceBean> getSinglePDF(String id){

        final MutableLiveData<AttendanceBean> newsData = new MutableLiveData<>();

        api.getSinglePDF(id).enqueue(new Callback<AttendanceBean>() {
            @Override
            public void onResponse(Call<AttendanceBean> call, Response<AttendanceBean> response) {
                if (response.isSuccessful()){
                    newsData.setValue(response.body());
                }
            }

            @Override
            public void onFailure(Call<AttendanceBean> call, Throwable t) {
                newsData.setValue(null);
            }
        });


        return newsData;
    }
}
