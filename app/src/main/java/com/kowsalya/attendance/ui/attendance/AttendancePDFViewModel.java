package com.kowsalya.attendance.ui.attendance;

import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.ViewModel;

import com.kowsalya.attendance.ui.students.StudentsRepository;
import com.kowsalya.attendance.ui.students.StudentsResponse;

public class AttendancePDFViewModel extends ViewModel {

    private MutableLiveData<AttendanceBean> mutableLiveDataAll;
    private MutableLiveData<AttendanceBean> mutableLiveDataSingle;
    private AttendancePDFRepository repository;

    public void initAll(String id,String name,String subject) {
        if (mutableLiveDataAll != null) {
            return;
        }
        repository = AttendancePDFRepository.getInstance();

        mutableLiveDataAll = repository.getAllPDF(id,name,subject);

    }

    public void initSingle(String id) {
        if (mutableLiveDataAll != null) {
            return;
        }
        repository = AttendancePDFRepository.getInstance();

        mutableLiveDataSingle = repository.getSinglePDF(id);

    }


    public LiveData<AttendanceBean> getAll() {
        return mutableLiveDataAll;
    }

    public LiveData<AttendanceBean> getSingle() {
        return mutableLiveDataSingle;
    }

}