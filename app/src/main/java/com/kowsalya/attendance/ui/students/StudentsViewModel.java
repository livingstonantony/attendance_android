package com.kowsalya.attendance.ui.students;

import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.ViewModel;

public class StudentsViewModel extends ViewModel {

    private MutableLiveData<String> mText;
    private MutableLiveData<StudentsResponse> mutableLiveData;
    private StudentsRepository studentsRepository;

    public void init(String id){
        if (mutableLiveData != null){
            return;
        }
        studentsRepository = StudentsRepository.getInstance();

        mutableLiveData = studentsRepository.getStudents(id);

    }
    public StudentsViewModel() {
        mText = new MutableLiveData<>();
        mText.setValue("This is attendance fragment");
    }


    public LiveData<StudentsResponse> getNewsRepository() {
        return mutableLiveData;
    }

}