package com.kowsalya.attendance.ui.activities;

import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProvider;

import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.text.Editable;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.EditText;

import com.bosco.attendance.R;
import com.kowsalya.attendance.ChooseActivity;
import com.kowsalya.attendance.MainActivity;
import com.kowsalya.attendance.interfaces.Config;
import com.kowsalya.attendance.ui.add_student.ResponseFromAPI;
import com.kowsalya.attendance.ui.satff_change_password.ChangePasswordBean;
import com.kowsalya.attendance.ui.satff_change_password.StaffChangePasswordViewModel;
import com.kowsalya.attendance.ui.student_change_password.StudentChangePasswordViewModel;
import com.kowsalya.attendance.utils.ApplicationSettings;
import com.kowsalya.attendance.utils.Utils;

public class StudentProfileActivity extends AppCompatActivity {
    private StudentChangePasswordViewModel viewModel;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_student_profile);

        viewModel = new ViewModelProvider(this).get(StudentChangePasswordViewModel.class);

        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setTitle("Student Profile");
//        NavController navController =

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
    }


    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        switch (item.getItemId()) {
            case android.R.id.home:
                onBackPressed();
                break;
            case R.id.menu_stu_change_password:
                getPassword();
                break;
            case R.id.menu_stu_logout:
                ApplicationSettings.clear();
                Intent intent = new Intent(this, ChooseActivity.class);
                intent.addFlags(Intent.FLAG_ACTIVITY_REORDER_TO_FRONT);
                startActivity(intent);
                break;
            case R.id.menu_delete:
                deleteMyAccount(ApplicationSettings.read(Config.STUDENT_ID, ""));
                break;

        }
        return super.onOptionsItemSelected(item);
    }

    private void deleteMyAccount(String id) {

        viewModel.init(ApplicationSettings.read(Config.STUDENT_ID, ""));
        viewModel.getResponseDelete().observe(StudentProfileActivity.this, new Observer<ResponseFromAPI>() {
            @Override
            public void onChanged(ResponseFromAPI responseFromAPI) {
                if (responseFromAPI != null) {
                    Utils.showMSG(StudentProfileActivity.this, "Student", "Account deleted successfully");
                    finish();
                } else {
                    Utils.showMSG(StudentProfileActivity.this, "Student", "There was some problem, Try again later");
                }
            }
        });
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_stu_profile, menu);
        return true;
    }

    private void getPassword() {
        AlertDialog dialogBox = Utils.showDialog(this);
        AlertDialog.Builder alert = new AlertDialog.Builder(this);
        final EditText edittext = new EditText(this);
        alert.setMessage("Password");
        alert.setTitle("Change your password");

        alert.setView(edittext);

        alert.setPositiveButton("CHANGE", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int whichButton) {
                //What ever you want to do with the value
                //OR
                String pwd = edittext.getText().toString();

                dialog.dismiss();

                dialogBox.show();

                Log.d("ID ", " " + ApplicationSettings.read(Config.ID, "") + " : " + pwd);

                viewModel.init(new ChangePasswordBean(ApplicationSettings.read(Config.STUDENT_ID, ""), pwd));

                viewModel.getResponse().observe(StudentProfileActivity.this, new Observer<ResponseFromAPI>() {
                    @Override
                    public void onChanged(ResponseFromAPI responseFromAPI) {
                        dialogBox.dismiss();

                        if (responseFromAPI != null) {
                            Utils.showMSG(StudentProfileActivity.this, "Password", "Password changed successfully");
                        } else {
                            Utils.showMSG(StudentProfileActivity.this, "Password", "There was some problem, Try again later");

                        }
                    }
                });

            }
        });

        alert.setNegativeButton("CANCEL", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int whichButton) {
                // what ever you want to do with No option.
            }
        });

        alert.show();
    }
}
