package com.kowsalya.attendance.ui.add_student;

import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.ViewModel;

import com.kowsalya.attendance.ui.student_login.StudentLoginResponse;

public class AddStudentViewModel extends ViewModel {

    private MutableLiveData<String> mText;
    private MutableLiveData<ResponseFromAPI> mutableLiveData;
    private MutableLiveData<StudentLoginResponse> liveData;
    private AddStudentRepository registerRepository;

    public void init(AddStudentPost registerPost) {
       /* if (mutableLiveData != null){
            return;
        }*/
        registerRepository = AddStudentRepository.getInstance();

        mutableLiveData = registerRepository.setRegister(registerPost);

    }

    public void getStudent(String id) {
       /* if (mutableLiveData != null){
            return;
        }*/
        registerRepository = AddStudentRepository.getInstance();

        liveData = registerRepository.getStudent(id);

    }

    public AddStudentViewModel() {
        mText = new MutableLiveData<>();
        mText.setValue("This is attendance fragment");
    }


    public LiveData<ResponseFromAPI> getRegisterResponse() {
        return mutableLiveData;
    }

    public LiveData<StudentLoginResponse> getStudentResponse() {
        return liveData;
    }
}

