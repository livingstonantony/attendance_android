package com.kowsalya.attendance.ui.staff_login;

import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.ViewModel;

import com.kowsalya.attendance.ui.add_student.ResponseFromAPI;
import com.kowsalya.attendance.ui.staff_register.StaffPost;
import com.kowsalya.attendance.ui.staff_register.StaffRegisterRepository;

public class StaffLoginViewModel extends ViewModel {

    private MutableLiveData<String> mText;
    private MutableLiveData<StaffLoginResponse> mutableLiveData;
    private StaffLoginRepository registerRepository;

    public void init(String id, String password) {
       /* if (mutableLiveData != null){
            return;
        }*/
        registerRepository = StaffLoginRepository.getInstance();

        mutableLiveData = registerRepository.getStaffProfile(id, password);

    }

    public StaffLoginViewModel() {
        mText = new MutableLiveData<>();
        mText.setValue("This is attendance fragment");
    }


    public LiveData<StaffLoginResponse> getRegisterResponse() {
        return mutableLiveData;
    }
}