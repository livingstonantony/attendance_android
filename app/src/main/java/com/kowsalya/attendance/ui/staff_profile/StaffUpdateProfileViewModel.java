package com.kowsalya.attendance.ui.staff_profile;

import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.ViewModel;

import com.kowsalya.attendance.ui.add_student.ResponseFromAPI;
import com.kowsalya.attendance.ui.student.StudentDetailsBean;
import com.kowsalya.attendance.ui.student.StudentProfileUpdateRepository;

public class StaffUpdateProfileViewModel extends ViewModel {

    private MutableLiveData<String> mText;
    private MutableLiveData<ResponseFromAPI> mutableLiveData;
    private StaffProfileUpdateRepository repository;

    public void init(StaffDetailsBean bean) {
       /* if (mutableLiveData != null){
            return;
        }*/
        repository = StaffProfileUpdateRepository.getInstance();

        mutableLiveData = repository.updateProfile(bean);

    }

    public StaffUpdateProfileViewModel() {
        mText = new MutableLiveData<>();
        mText.setValue("This is attendance fragment");
    }


    public LiveData<ResponseFromAPI> getRegisterResponse() {
        return mutableLiveData;
    }
}