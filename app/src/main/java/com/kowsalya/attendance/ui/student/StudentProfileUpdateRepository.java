package com.kowsalya.attendance.ui.student;


import androidx.lifecycle.MutableLiveData;

import com.kowsalya.attendance.retrofit.RetrofitService;
import com.kowsalya.attendance.ui.add_student.AddStudentApi;
import com.kowsalya.attendance.ui.add_student.AddStudentPost;
import com.kowsalya.attendance.ui.add_student.ResponseFromAPI;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class StudentProfileUpdateRepository {

    private static StudentProfileUpdateRepository registerRepository;

    public static StudentProfileUpdateRepository getInstance() {
        if (registerRepository == null) {
            registerRepository = new StudentProfileUpdateRepository();
        }
        return registerRepository;
    }

    private StudentProfileUpdateApi registerApi;

    public StudentProfileUpdateRepository() {
        registerApi = RetrofitService.createService(StudentProfileUpdateApi.class);
    }


    public MutableLiveData<ResponseFromAPI> updateProfile(StudentDetailsBean bean) {
        final MutableLiveData<ResponseFromAPI> mutableLiveData = new MutableLiveData<>();

        registerApi.updateStudent(bean).enqueue(new Callback<ResponseFromAPI>() {
            @Override
            public void onResponse(Call<ResponseFromAPI> call, Response<ResponseFromAPI> response) {
                if (response.isSuccessful()) {

                    mutableLiveData.setValue(response.body());
//                    mutableLiveData.setValue(null);
                }
            }

            @Override
            public void onFailure(Call<ResponseFromAPI> call, Throwable t) {
                mutableLiveData.setValue(null);
            }
        });

        return mutableLiveData;
    }
}
