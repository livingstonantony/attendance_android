package com.kowsalya.attendance.ui.staff_profile;


import com.kowsalya.attendance.ui.add_student.ResponseFromAPI;
import com.kowsalya.attendance.ui.student.StudentDetailsBean;

import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.Headers;
import retrofit2.http.POST;

public interface StaffProfileUpdateApi {
    @Headers( "Content-Type: application/json" )
    @POST("update_staff")
    Call<ResponseFromAPI> updateStudent(@Body StaffDetailsBean bean);
}
