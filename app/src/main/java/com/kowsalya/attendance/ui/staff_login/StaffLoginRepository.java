package com.kowsalya.attendance.ui.staff_login;


import androidx.lifecycle.MutableLiveData;

import com.kowsalya.attendance.retrofit.RetrofitService;
import com.kowsalya.attendance.ui.add_student.ResponseFromAPI;
import com.kowsalya.attendance.ui.staff_register.StaffPost;
import com.kowsalya.attendance.ui.staff_register.StaffRegisterApi;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class StaffLoginRepository {

    private static StaffLoginRepository registerRepository;

    public static StaffLoginRepository getInstance() {
        if (registerRepository == null) {
            registerRepository = new StaffLoginRepository();
        }
        return registerRepository;
    }

    private StaffLoginApi registerApi;

    public StaffLoginRepository() {
        registerApi = RetrofitService.createService(StaffLoginApi.class);
    }


    public MutableLiveData<StaffLoginResponse> getStaffProfile(String id, String password) {
        final MutableLiveData<StaffLoginResponse> mutableLiveData = new MutableLiveData<>();

        registerApi.getStaffProfile(id, password).enqueue(new Callback<StaffLoginResponse>() {
            @Override
            public void onResponse(Call<StaffLoginResponse> call, Response<StaffLoginResponse> response) {
                if (response.isSuccessful()) {
                    mutableLiveData.setValue(response.body());
                }
            }

            @Override
            public void onFailure(Call<StaffLoginResponse> call, Throwable t) {
                mutableLiveData.setValue(null);
            }
        });

        return mutableLiveData;
    }
}
