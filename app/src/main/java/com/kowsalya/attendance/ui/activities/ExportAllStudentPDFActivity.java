package com.kowsalya.attendance.ui.activities;

import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProvider;
import androidx.recyclerview.widget.RecyclerView;

import android.os.Bundle;
import android.view.MenuItem;
import android.view.View;
import android.widget.TextView;

import com.bosco.attendance.R;
import com.kowsalya.attendance.interfaces.Config;
import com.kowsalya.attendance.ui.attendance.AttendanceAdapter;
import com.kowsalya.attendance.ui.attendance.AttendanceBean;
import com.kowsalya.attendance.ui.attendance.AttendancePDFViewModel;
import com.kowsalya.attendance.ui.student_change_password.StudentChangePasswordViewModel;
import com.kowsalya.attendance.utils.ApplicationSettings;
import com.kowsalya.attendance.utils.Utils;

import java.util.ArrayList;
import java.util.List;

public class ExportAllStudentPDFActivity extends AppCompatActivity {

    private AttendancePDFViewModel viewModel;

    private RecyclerView recyclerView;
    private List<AttendanceBean.AttendanceData> list;

    private AttendanceAdapter  adapter;
    AlertDialog dialog;
    private TextView mTextViwNoData;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_export_all_student_p_d_f);

        Toolbar toolbar = findViewById(R.id.toolbar2);
        setSupportActionBar(toolbar);
        getSupportActionBar().setTitle("PDF");
//        NavController navController =
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);


        list = new ArrayList<>();
        dialog = Utils.showDialog(this);
        recyclerView = findViewById(R.id.recyclerViewAttendance);
        adapter = new AttendanceAdapter(list,"staff");
        recyclerView.setAdapter(adapter);



        mTextViwNoData = findViewById(R.id.textViewNoData);
        viewModel = new ViewModelProvider(this).get(AttendancePDFViewModel.class);

        viewModel.initAll(ApplicationSettings.read(Config.ID,""),
                ApplicationSettings.read(Config.NAME,""),
                ApplicationSettings.read(Config.SUBJECT,""));
        viewModel.getAll().observe(this, new Observer<AttendanceBean>() {
            @Override
            public void onChanged(AttendanceBean attendanceBean) {
                dialog.dismiss();
                if (attendanceBean != null) {
                    if (attendanceBean.getList().size() != 0) {
                        mTextViwNoData.setVisibility(View.GONE);
                        list.clear();

                        for (AttendanceBean.AttendanceData data : attendanceBean.getList()){
                            if (data.getName().contains("staff")){
                                list.add(data);
                            }
                        }

                        adapter.notifyDataSetChanged();
                    }
                }
            }
        });
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        switch (item.getItemId()) {
            case android.R.id.home:
                onBackPressed();
                break;
        }

        return super.onOptionsItemSelected(item);
    }
}