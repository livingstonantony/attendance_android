package com.kowsalya.attendance.ui.mark_attendance;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class AttendanceList {

    @SerializedName("attendance")
    @Expose
    private List<String> list;

    @SerializedName("staff_id")
    @Expose
    private String id;

    public AttendanceList(List<String> list, String id) {
        this.list = list;
        this.id = id;
    }


    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public AttendanceList(List<String> list) {
        this.list = list;
    }

    public List<String> getList() {
        return list;
    }

    public void setList(List<String> list) {
        this.list = list;
    }
}
