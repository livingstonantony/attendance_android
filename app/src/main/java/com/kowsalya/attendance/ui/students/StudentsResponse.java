package com.kowsalya.attendance.ui.students;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class StudentsResponse {


    @SerializedName("students")
    @Expose
    private List<Student> list;

    public StudentsResponse(List<Student> list) {
        this.list = list;
    }

    public List<Student> getList() {
        return list;
    }

    public void setList(List<Student> list) {
        this.list = list;
    }
}
