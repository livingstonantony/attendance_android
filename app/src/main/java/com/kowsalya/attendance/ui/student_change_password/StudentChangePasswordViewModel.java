package com.kowsalya.attendance.ui.student_change_password;

import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.ViewModel;

import com.kowsalya.attendance.ui.add_student.ResponseFromAPI;
import com.kowsalya.attendance.ui.satff_change_password.ChangePasswordBean;
import com.kowsalya.attendance.ui.satff_change_password.StaffChangePasswordRepository;

public class StudentChangePasswordViewModel extends ViewModel {

    private MutableLiveData<String> mText;
    private MutableLiveData<ResponseFromAPI> mutableLiveData;
    private MutableLiveData<ResponseFromAPI> mutableLiveDataDelete;
    private StudentChangePasswordRepository repository;

    public void init(ChangePasswordBean bean) {
       /* if (mutableLiveData != null){
            return;
        }*/
        repository = StudentChangePasswordRepository.getInstance();

        mutableLiveData = repository.setPassword(bean);

    }

    public void init(String id) {
       /* if (mutableLiveData != null){
            return;
        }*/
        repository = StudentChangePasswordRepository.getInstance();

        mutableLiveDataDelete = repository.deleteAccount(id);

    }

    public StudentChangePasswordViewModel() {
        mText = new MutableLiveData<>();
        mText.setValue("This is attendance fragment");
    }


    public LiveData<ResponseFromAPI> getResponse() {
        return mutableLiveData;
    }

    public LiveData<ResponseFromAPI> getResponseDelete() {
        return mutableLiveDataDelete;
    }
}