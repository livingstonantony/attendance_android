package com.kowsalya.attendance.ui.students;


import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Query;

public interface StudentsApi {
    @GET("all_students")
    Call<StudentsResponse> getStudentsList(@Query("id") String id);
}
