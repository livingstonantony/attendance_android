package com.kowsalya.attendance.ui.add_student;

import android.Manifest;
import android.content.Intent;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.os.StrictMode;
import android.provider.MediaStore;
import android.provider.Settings;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AlertDialog;
import androidx.core.content.FileProvider;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProvider;

import com.bosco.attendance.R;
import com.kowsalya.attendance.interfaces.Config;
import com.kowsalya.attendance.ui.student_login.StudentLoginResponse;
import com.kowsalya.attendance.utils.ApplicationSettings;
import com.kowsalya.attendance.utils.EncodeBased64Binary;
import com.kowsalya.attendance.utils.FileCompressor;
import com.kowsalya.attendance.utils.Utils;
import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;
import com.google.android.material.button.MaterialButton;
import com.karumi.dexter.Dexter;
import com.karumi.dexter.MultiplePermissionsReport;
import com.karumi.dexter.PermissionToken;
import com.karumi.dexter.listener.PermissionRequest;
import com.karumi.dexter.listener.multi.MultiplePermissionsListener;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.concurrent.ExecutionException;

import static android.app.Activity.RESULT_OK;

public class AddStudentFragment extends Fragment {

    private AddStudentViewModel registerViewModel;

    private ImageView mImageViewAdd;
    private ImageView mImageViewProfile;

    static final int REQUEST_TAKE_PHOTO = 1;
    static final int REQUEST_GALLERY_PHOTO = 2;
    private File mPhotoFile;
    private FileCompressor mCompressor;

    private MaterialButton mButtonRegister;

    private EditText mEditTextId, mEditTexName;
    private EditText mEditTextCourse;
    private RadioButton mRadioButton;
    private RadioButton mRadioButtonFemale;

    private RadioGroup mRadioGroup;

    private boolean isStudentExist;

    public View onCreateView(@NonNull LayoutInflater inflater,
                             ViewGroup container, Bundle savedInstanceState) {
        View root = inflater.inflate(R.layout.fragment_add_student, container, false);

        return root;
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder().permitAll().build();
        StrictMode.setThreadPolicy(policy);

        registerViewModel = new ViewModelProvider(this).get(AddStudentViewModel.class);

        mCompressor = new FileCompressor(getContext());

        mEditTextId = view.findViewById(R.id.editTextId);
        mEditTexName = view.findViewById(R.id.editTextName);
        mEditTextCourse = view.findViewById(R.id.editTextSubject);
        mRadioButton = view.findViewById(R.id.radioButton);
        mRadioButtonFemale = view.findViewById(R.id.second);

        mRadioGroup = view.findViewById(R.id.radioGroup);

        mImageViewAdd = view.findViewById(R.id.imageViewAddProfile);
        mImageViewProfile = view.findViewById(R.id.imageViewProfile);


        mEditTextId.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View v, boolean hasFocus) {
                if (!hasFocus) {
                    registerViewModel.getStudent(mEditTextId.getText().toString().trim());

                    registerViewModel.getStudentResponse().observe(getViewLifecycleOwner(), new Observer<StudentLoginResponse>() {
                        @Override
                        public void onChanged(StudentLoginResponse response) {
                            if (response != null) {

                                if (response.getProfileData() != null) {

                                    StudentLoginResponse.ProfileData data = response.getProfileData();

                                    if (data.getId() != null) {

                                        isStudentExist = true;


                                        mEditTextId.setFocusable(false);
                                        mEditTexName.setFocusable(false);
                                        mEditTextCourse.setFocusable(false);
                                        mRadioButton.setClickable(false);
                                        mRadioButtonFemale.setClickable(false);

                                        mEditTextId.setText(data.getId());
                                    }
                                    if (data.getName() != null) {
                                        mEditTexName.setText(data.getName());
                                    }

                                    if (data.getSubject() != null) {
                                        mEditTextCourse.setText(data.getSubject());
                                    }

                                    if (data.getProfile() != null) {
                                        if (getContext() != null) {
                                            Glide.with(getContext()).load(data.getProfile()).apply(new RequestOptions().placeholder(R.drawable.person)).into(mImageViewProfile);
                                        }
                                    }
                                }
                            }
                        }
                    });
                }
            }
        });

        mImageViewProfile.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
//                Glide.with(getContext()).load("http://10.0.3.2:80/images/package.png").apply(new RequestOptions().placeholder(R.drawable.person)).into(mImageViewProfile);

                if (mPhotoFile != null) {
                    Utils.viewImage(getContext(), mPhotoFile);
                } else {
                    Toast.makeText(getContext(), "Choose profile picture...!", Toast.LENGTH_SHORT).show();
                }
            }
        });

        mButtonRegister = view.findViewById(R.id.buttonRegister);

        mImageViewAdd.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                selectImage();
            }
        });

        mButtonRegister.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (mPhotoFile != null || isStudentExist) {
                    String id = mEditTextId.getText().toString().trim();
                    String name = mEditTexName.getText().toString().trim();
                    String course = mEditTextCourse.getText().toString();
                    int gender = 0;

                    if (mRadioButton.isChecked()) {
                        gender = 0;
                    } else {
                        gender = 1;
                    }
                    String image = "";

                    if (!isStudentExist) {
                        Bitmap myBitmap = BitmapFactory.decodeFile(mPhotoFile.getAbsolutePath());


                        ByteArrayOutputStream outputStream = new ByteArrayOutputStream();
                        myBitmap.compress(Bitmap.CompressFormat.JPEG, 0, outputStream);


                        try {
                            image = EncodeBased64Binary.encodeFileToBase64Binary(mPhotoFile);
                        } catch (IOException e) {
                            e.printStackTrace();
                        }
                    }

                    AddStudentPost registerPost = new AddStudentPost(id, name, image, course, gender, ApplicationSettings.read(Config.ID, ""), ApplicationSettings.read(Config.SUBJECT, ""));

                    registerViewModel.init(registerPost);
                    registerViewModel.getRegisterResponse().observe(getViewLifecycleOwner(), new Observer<ResponseFromAPI>() {
                        @Override
                        public void onChanged(ResponseFromAPI registerResponse) {
//                            Toast.makeText(getContext(), "" + registerResponse, Toast.LENGTH_SHORT).show();
                            Utils.showMSG(getActivity(), "Student", registerResponse.getResponse());
                        }
                    });
                } else {
                    Toast.makeText(getContext(), "Choose profile image..!", Toast.LENGTH_SHORT).show();
                }


            }
        });

    }


    /**
     * Convert to bitmap from url
     *
     * @param url1
     * @return
     */
    public static Bitmap getBitmapFromURL(String url1) {
        try {
            URL url = new URL(url1);
            HttpURLConnection connection = (HttpURLConnection) url.openConnection();
            connection.setDoInput(true);
            connection.connect();
            InputStream input = connection.getInputStream();
            Bitmap bitmapFrmUrl = BitmapFactory.decodeStream(input);
            return bitmapFrmUrl;
        } catch (IOException e) {
            e.printStackTrace();
            return null;
        }
    }

    /**
     * Alert dialog for capture or select from galley
     */
    private void selectImage() {
        final CharSequence[] items = {"Take Photo", "Choose from Library",
                "Cancel"};
        AlertDialog.Builder builder = new AlertDialog.Builder(getContext());
        builder.setItems(items, (dialog, item) -> {
            if (items[item].equals("Take Photo")) {
                requestStoragePermission(true);
            } else if (items[item].equals("Choose from Library")) {
                requestStoragePermission(false);
            } else if (items[item].equals("Cancel")) {
                dialog.dismiss();
            }
        });
        builder.show();
    }

    /**
     * Capture image from camera
     */
    private void dispatchTakePictureIntent() {
        Intent takePictureIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
        if (takePictureIntent.resolveActivity(getActivity().getPackageManager()) != null) {
            // Create the File where the photo should go
            File photoFile = null;
            try {
                photoFile = createImageFile();
            } catch (IOException ex) {
                ex.printStackTrace();
                // Error occurred while creating the File
            }
            if (photoFile != null) {
                Uri photoURI = FileProvider.getUriForFile(getActivity().getApplicationContext(),
                        "com.bosco.attendance.provider",
                        photoFile);

                mPhotoFile = photoFile;
                takePictureIntent.putExtra(MediaStore.EXTRA_OUTPUT, photoURI);
                startActivityForResult(takePictureIntent, REQUEST_TAKE_PHOTO);

            }
        }
    }


    /**
     * Select image fro gallery
     */
    private void dispatchGalleryIntent() {
        Intent pickPhoto = new Intent(Intent.ACTION_PICK,
                MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
        pickPhoto.addFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION);
        startActivityForResult(pickPhoto, REQUEST_GALLERY_PHOTO);
    }


    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (resultCode == RESULT_OK) {
            if (requestCode == REQUEST_TAKE_PHOTO) {
                try {
                    mPhotoFile = mCompressor.compressToFile(mPhotoFile);
                } catch (IOException e) {
                    e.printStackTrace();
                }
                Glide.with(getContext()).load(mPhotoFile).apply(new RequestOptions().centerCrop().circleCrop().placeholder(R.drawable.person)).into(mImageViewProfile);
            } else if (requestCode == REQUEST_GALLERY_PHOTO) {
                Uri selectedImage = data.getData();
                try {
                    mPhotoFile = mCompressor.compressToFile(new File(getRealPathFromUri(selectedImage)));
                } catch (IOException e) {
                    e.printStackTrace();
                }
                Glide.with(getContext()).load(mPhotoFile).apply(new RequestOptions().centerCrop().circleCrop().placeholder(R.drawable.person)).into(mImageViewProfile);

            }
        }
    }

    /**
     * Requesting multiple permissions (storage and camera) at once
     * This uses multiple permission model from dexter
     * On permanent denial opens settings dialog
     */
    private void requestStoragePermission(boolean isCamera) {
        Dexter.withActivity(getActivity()).withPermissions(Manifest.permission.READ_EXTERNAL_STORAGE, Manifest.permission.WRITE_EXTERNAL_STORAGE, Manifest.permission.CAMERA)
                .withListener(new MultiplePermissionsListener() {
                    @Override
                    public void onPermissionsChecked(MultiplePermissionsReport report) {
                        // check if all permissions are granted
                        if (report.areAllPermissionsGranted()) {
                            if (isCamera) {
                                dispatchTakePictureIntent();
                            } else {
                                dispatchGalleryIntent();
                            }
                        }
                        // check for permanent denial of any permission
                        if (report.isAnyPermissionPermanentlyDenied()) {
                            // show alert dialog navigating to Settings
                            showSettingsDialog();
                        }
                    }

                    @Override
                    public void onPermissionRationaleShouldBeShown(List<PermissionRequest> permissions, PermissionToken token) {
                        token.continuePermissionRequest();
                    }
                }).withErrorListener(error -> Toast.makeText(getActivity().getApplicationContext(), "Error occurred! ", Toast.LENGTH_SHORT).show())
                .onSameThread()
                .check();
    }


    /**
     * Showing Alert Dialog with Settings option
     * Navigates user to app settings
     * NOTE: Keep proper title and message depending on your app
     */
    private void showSettingsDialog() {
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        builder.setTitle("Need Permissions");
        builder.setMessage("This app needs permission to use this feature. You can grant them in app settings.");
        builder.setPositiveButton("GOTO SETTINGS", (dialog, which) -> {
            dialog.cancel();
            openSettings();
        });
        builder.setNegativeButton("Cancel", (dialog, which) -> dialog.cancel());
        builder.show();

    }

    // navigating user to app settings
    private void openSettings() {
        Intent intent = new Intent(Settings.ACTION_APPLICATION_DETAILS_SETTINGS);
        Uri uri = Uri.fromParts("package", getActivity().getPackageName(), null);
        intent.setData(uri);
        startActivityForResult(intent, 101);
    }

    /**
     * Create file with current timestamp name
     *
     * @return
     * @throws IOException
     */
    private File createImageFile() throws IOException {
        // Create an image file name
        String timeStamp = new SimpleDateFormat("yyyyMMddHHmmss").format(new Date());
        String mFileName = "JPG_" + timeStamp + "_";
        File storageDir = getActivity().getExternalFilesDir(Environment.DIRECTORY_PICTURES);
        File mFile = File.createTempFile(mFileName, ".jpg", storageDir);
        return mFile;
    }

    /**
     * Get real file path from URI
     *
     * @param contentUri
     * @return
     */
    public String getRealPathFromUri(Uri contentUri) {
        Cursor cursor = null;
        try {
            String[] proj = {MediaStore.Images.Media.DATA};
            cursor = getActivity().getContentResolver().query(contentUri, proj, null, null, null);
            assert cursor != null;
            int column_index = cursor.getColumnIndexOrThrow(MediaStore.Images.Media.DATA);
            cursor.moveToFirst();
            return cursor.getString(column_index);
        } finally {
            if (cursor != null) {
                cursor.close();
            }
        }
    }


}
