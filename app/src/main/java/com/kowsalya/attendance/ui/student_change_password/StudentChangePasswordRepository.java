package com.kowsalya.attendance.ui.student_change_password;


import androidx.lifecycle.MutableLiveData;

import com.kowsalya.attendance.retrofit.RetrofitService;
import com.kowsalya.attendance.ui.add_student.ResponseFromAPI;
import com.kowsalya.attendance.ui.satff_change_password.ChangePasswordBean;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class StudentChangePasswordRepository {

    private static StudentChangePasswordRepository registerRepository;

    public static StudentChangePasswordRepository getInstance() {
        if (registerRepository == null) {
            registerRepository = new StudentChangePasswordRepository();
        }
        return registerRepository;
    }

    private StudentChangePasswordApi api;

    public StudentChangePasswordRepository() {
        api = RetrofitService.createService(StudentChangePasswordApi.class);
    }


    public MutableLiveData<ResponseFromAPI> deleteAccount(String id) {
        final MutableLiveData<ResponseFromAPI> mutableLiveData = new MutableLiveData<>();

        api.deleteAccount(id).enqueue(new Callback<ResponseFromAPI>() {
            @Override
            public void onResponse(Call<ResponseFromAPI> call, Response<ResponseFromAPI> response) {
                mutableLiveData.setValue(response.body());
            }

            @Override
            public void onFailure(Call<ResponseFromAPI> call, Throwable t) {
                mutableLiveData.setValue(null);
            }
        });


        return mutableLiveData;
    }
    public MutableLiveData<ResponseFromAPI> setPassword(ChangePasswordBean bean) {
        final MutableLiveData<ResponseFromAPI> mutableLiveData = new MutableLiveData<>();

        api.setPassword(bean).enqueue(new Callback<ResponseFromAPI>() {
            @Override
            public void onResponse(Call<ResponseFromAPI> call, Response<ResponseFromAPI> response) {
                if (response.isSuccessful()) {

                    mutableLiveData.setValue(response.body());
//                    mutableLiveData.setValue(null);
                }
            }

            @Override
            public void onFailure(Call<ResponseFromAPI> call, Throwable t) {
                mutableLiveData.setValue(null);
            }
        });

        return mutableLiveData;
    }
}
