package com.kowsalya.attendance.ui.student;


import com.kowsalya.attendance.ui.add_student.AddStudentPost;
import com.kowsalya.attendance.ui.add_student.ResponseFromAPI;

import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.Headers;
import retrofit2.http.POST;

public interface StudentProfileUpdateApi {
    @Headers( "Content-Type: application/json" )
    @POST("update_student")
    Call<ResponseFromAPI> updateStudent(@Body StudentDetailsBean bean);
}
