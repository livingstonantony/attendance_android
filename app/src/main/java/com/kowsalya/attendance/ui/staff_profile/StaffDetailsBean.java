package com.kowsalya.attendance.ui.staff_profile;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class StaffDetailsBean {


    @SerializedName("id")
    @Expose
    private String id;

    @SerializedName("name")
    @Expose
    private String name;

    @SerializedName("subject")
    @Expose
    private String subject;

    public StaffDetailsBean(String id, String name, String subject) {
        this.id = id;
        this.name = name;
        this.subject = subject;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getSubject() {
        return subject;
    }

    public void setSubject(String subject) {
        this.subject = subject;
    }
}
