package com.kowsalya.attendance.ui.mark_attendance;

import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.ViewModel;

import com.kowsalya.attendance.ui.add_student.ResponseFromAPI;

public class MarkAttendanceViewModel extends ViewModel {

    private MutableLiveData<String> mText;
    private MutableLiveData<ResponseFromAPI> mutableLiveData;
    private AttendanceRepository repository;

    public void init(AttendanceList list){
       /* if (mutableLiveData != null){
            return;
        }*/
        repository = AttendanceRepository.getInstance();

        mutableLiveData = repository.setAttendance(list);

    }
    public MarkAttendanceViewModel() {
        mText = new MutableLiveData<>();
        mText.setValue("This is attendance fragment");
    }


    public LiveData<ResponseFromAPI> getAttendanceResponse() {
        return mutableLiveData;
    }

}