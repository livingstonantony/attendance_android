package com.kowsalya.attendance.ui.staff_register;

import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.ViewModel;

import com.kowsalya.attendance.ui.add_student.AddStudentPost;
import com.kowsalya.attendance.ui.add_student.AddStudentRepository;
import com.kowsalya.attendance.ui.add_student.ResponseFromAPI;

public class StaffRegistertViewModel extends ViewModel {

    private MutableLiveData<String> mText;
    private MutableLiveData<ResponseFromAPI> mutableLiveData;
    private StaffRegisterRepository registerRepository;

    public void init(StaffPost registerPost) {
       /* if (mutableLiveData != null){
            return;
        }*/
        registerRepository = StaffRegisterRepository.getInstance();

        mutableLiveData = registerRepository.setRegister(registerPost);

    }

    public StaffRegistertViewModel() {
        mText = new MutableLiveData<>();
        mText.setValue("This is attendance fragment");
    }


    public LiveData<ResponseFromAPI> getRegisterResponse() {
        return mutableLiveData;
    }
}