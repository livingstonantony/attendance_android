package com.kowsalya.attendance.ui.satff_change_password;


import com.kowsalya.attendance.ui.add_student.ResponseFromAPI;

import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.POST;

public interface StaffChangePasswordApi {
    @POST("change_staff_password")
    Call<ResponseFromAPI> setPassword(@Body ChangePasswordBean bean);
}
