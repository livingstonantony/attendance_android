package com.kowsalya.attendance.ui.staff_register;


import com.kowsalya.attendance.ui.add_student.ResponseFromAPI;
import com.kowsalya.attendance.ui.mark_attendance.AttendanceList;

import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.POST;

public interface StaffRegisterApi {
    @POST("register_staff")
    Call<ResponseFromAPI> setStaff(@Body StaffPost post);
}
