package com.kowsalya.attendance.ui.satff_change_password;

import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.ViewModel;

import com.kowsalya.attendance.ui.add_student.ResponseFromAPI;

public class StaffChangePasswordViewModel extends ViewModel {

    private MutableLiveData<String> mText;
    private MutableLiveData<ResponseFromAPI> mutableLiveData;
    private StaffChangePasswordRepository repository;

    public void init(ChangePasswordBean bean) {
       /* if (mutableLiveData != null){
            return;
        }*/
        repository = StaffChangePasswordRepository.getInstance();

        mutableLiveData = repository.setPassword(bean);

    }

    public StaffChangePasswordViewModel() {
        mText = new MutableLiveData<>();
        mText.setValue("This is attendance fragment");
    }


    public LiveData<ResponseFromAPI> getResponse() {
        return mutableLiveData;
    }

}