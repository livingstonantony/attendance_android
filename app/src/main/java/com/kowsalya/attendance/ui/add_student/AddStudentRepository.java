package com.kowsalya.attendance.ui.add_student;


import androidx.lifecycle.MutableLiveData;

import com.kowsalya.attendance.retrofit.RetrofitService;
import com.kowsalya.attendance.ui.student_login.StudentLoginResponse;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class AddStudentRepository {

    private static AddStudentRepository registerRepository;

    public static AddStudentRepository getInstance() {
        if (registerRepository == null) {
            registerRepository = new AddStudentRepository();
        }
        return registerRepository;
    }

    private AddStudentApi registerApi;

    public AddStudentRepository() {
        registerApi = RetrofitService.createService(AddStudentApi.class);
    }

    public MutableLiveData<StudentLoginResponse> getStudent(String id){
        final  MutableLiveData<StudentLoginResponse> liveData = new MutableLiveData<>();

        registerApi.getStudent(id).enqueue(new Callback<StudentLoginResponse>() {
            @Override
            public void onResponse(Call<StudentLoginResponse> call, Response<StudentLoginResponse> response) {
                if (response.isSuccessful()) {
                    liveData.setValue(response.body());
                }
            }

            @Override
            public void onFailure(Call<StudentLoginResponse> call, Throwable t) {
                liveData.setValue(null);
            }
        });

        return liveData;
    }

    public MutableLiveData<ResponseFromAPI> setRegister(AddStudentPost registerPost) {
        final MutableLiveData<ResponseFromAPI> mutableLiveData = new MutableLiveData<>();

        registerApi.registerStudent(registerPost).enqueue(new Callback<ResponseFromAPI>() {
            @Override
            public void onResponse(Call<ResponseFromAPI> call, Response<ResponseFromAPI> response) {
                if (response.isSuccessful()) {

                    mutableLiveData.setValue(response.body());
//                    mutableLiveData.setValue(null);
                }
            }

            @Override
            public void onFailure(Call<ResponseFromAPI> call, Throwable t) {
                mutableLiveData.setValue(null);
            }
        });

        return mutableLiveData;
    }
}
