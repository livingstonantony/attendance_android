package com.kowsalya.attendance.ui.add_student;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class AddStudentPost {

    @SerializedName("id")
    @Expose
    private String id;

    @SerializedName("name")
    @Expose
    private String name;

    @SerializedName("img")
    @Expose
    private String image;

    @SerializedName("course")
    @Expose
    private String course;

    @SerializedName("gender")
    @Expose
    private int gender;

    @SerializedName("staff_id")
    @Expose
    private String stfID;

    @SerializedName("subject")
    @Expose
    private String subject;

    public AddStudentPost(String id, String name, String image, String course, int gender, String stfID, String subject) {
        this.id = id;
        this.name = name;
        this.image = image;
        this.course = course;
        this.gender = gender;
        this.stfID = stfID;
        this.subject = subject;
    }

    public String getStfID() {
        return stfID;
    }

    public String getSubject() {
        return subject;
    }

    public void setSubject(String subject) {
        this.subject = subject;
    }

    public void setStfID(String stfID) {
        this.stfID = stfID;
    }

    public String getCourse() {
        return course;
    }

    public void setCourse(String course) {
        this.course = course;
    }

    public int getGender() {
        return gender;
    }

    public void setGender(int gender) {
        this.gender = gender;
    }

    public AddStudentPost(String id, String name, String image) {
        this.id = id;
        this.name = name;
        this.image = image;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }
}
