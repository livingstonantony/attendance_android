package com.kowsalya.attendance.ui.students;


import androidx.lifecycle.MutableLiveData;


import com.kowsalya.attendance.retrofit.RetrofitService;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class StudentsRepository {

    private static StudentsRepository newsRepository;

    public static StudentsRepository getInstance(){
        if (newsRepository == null){
            newsRepository = new StudentsRepository();
        }
        return newsRepository;
    }

    private StudentsApi studentsApi;

    public StudentsRepository(){
        studentsApi = RetrofitService.createService(StudentsApi.class);
    }

    public MutableLiveData<StudentsResponse> getStudents(String id){

        final MutableLiveData<StudentsResponse> newsData = new MutableLiveData<>();


        studentsApi.getStudentsList(id).enqueue(new Callback<StudentsResponse>() {
            @Override
            public void onResponse(Call<StudentsResponse> call, Response<StudentsResponse> response) {
                if (response.isSuccessful()){
                    newsData.setValue(response.body());
                }
            }

            @Override
            public void onFailure(Call<StudentsResponse> call, Throwable t) {
                newsData.setValue(null);
            }
        });
        return newsData;
    }
}
