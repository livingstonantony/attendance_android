package com.kowsalya.attendance.ui.staff_register;


import androidx.lifecycle.MutableLiveData;

import com.kowsalya.attendance.retrofit.RetrofitService;
import com.kowsalya.attendance.ui.add_student.AddStudentApi;
import com.kowsalya.attendance.ui.add_student.AddStudentPost;
import com.kowsalya.attendance.ui.add_student.ResponseFromAPI;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class StaffRegisterRepository {

    private static StaffRegisterRepository registerRepository;

    public static StaffRegisterRepository getInstance() {
        if (registerRepository == null) {
            registerRepository = new StaffRegisterRepository();
        }
        return registerRepository;
    }

    private StaffRegisterApi registerApi;

    public StaffRegisterRepository() {
        registerApi = RetrofitService.createService(StaffRegisterApi.class);
    }


    public MutableLiveData<ResponseFromAPI> setRegister(StaffPost registerPost) {
        final MutableLiveData<ResponseFromAPI> mutableLiveData = new MutableLiveData<>();

        registerApi.setStaff(registerPost).enqueue(new Callback<ResponseFromAPI>() {
            @Override
            public void onResponse(Call<ResponseFromAPI> call, Response<ResponseFromAPI> response) {
                if (response.isSuccessful()) {

                    mutableLiveData.setValue(response.body());
//                    mutableLiveData.setValue(null);
                }
            }

            @Override
            public void onFailure(Call<ResponseFromAPI> call, Throwable t) {
                mutableLiveData.setValue(null);
            }
        });

        return mutableLiveData;
    }
}
