package com.kowsalya.attendance.ui.students;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.navigation.Navigation;
import androidx.recyclerview.widget.RecyclerView;

import com.bosco.attendance.R;
import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;
import com.kowsalya.attendance.interfaces.Config;
import com.kowsalya.attendance.utils.ApplicationSettings;
import com.kowsalya.attendance.utils.Utils;

import java.util.List;

public class StudentAdapter extends RecyclerView.Adapter<StudentAdapter.MyViewHolder> {

    private List<Student> list;

    public StudentAdapter(List<Student> list) {
        this.list = list;
    }

    @NonNull
    @Override
    public StudentAdapter.MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {

        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.layout_student_item, parent, false);

        return new MyViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull StudentAdapter.MyViewHolder holder, int position) {
        holder.bind(list.get(position));
    }

    @Override
    public int getItemCount() {
        return list.size();
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {

        TextView mId;
        TextView mName;
        TextView mAttendance;
        TextView mTime;
        ImageView imageView;

        public MyViewHolder(@NonNull View itemView) {
            super(itemView);

            mId = itemView.findViewById(R.id.textViewId);
            mName = itemView.findViewById(R.id.textViewName);
            mAttendance = itemView.findViewById(R.id.textViewAttendance);
            mTime = itemView.findViewById(R.id.textViewTime);
            imageView = itemView.findViewById(R.id.profile_image);
        }

        public void bind(Student data) {

            mId.setText(data.getStudentId());
            mName.setText(data.getStudentName());

            if (data.getStudentAttendance() == 1) {
                mAttendance.setText("Present");
                mAttendance.setTextColor(itemView.getContext().getResources().getColor(R.color.colorGreen));
            } else {
                mAttendance.setTextColor(itemView.getContext().getResources().getColor(R.color.colorRed));
                mAttendance.setText("Absent");
            }

            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    ApplicationSettings.write(Config.STUDENT_PROFILE, data.getStudentImgUrl());
                    ApplicationSettings.write(Config.STUDENT_ID, data.getStudentId());
                    ApplicationSettings.write(Config.STUDENT_NAME, data.getStudentName());
                    ApplicationSettings.write(Config.STUDENT_COURSE, data.getCourse());
                    ApplicationSettings.write(Config.STUDENT_GENDER, data.getGender());

                    Navigation.findNavController(v).navigate(R.id.action_nav_student_to_studentProfileFragment);
                }
            });

            mTime.setText(Utils.getDate(data.getStudentTime()));

            imageView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
//                    Utils.viewImage(v.getContext(),new File(data.getStudentImgUrl()));
                }
            });
            Glide.with(itemView.getContext()).load(data.getStudentImgUrl()).apply(new RequestOptions().placeholder(R.drawable.person)).into(imageView);


        }
    }
}
