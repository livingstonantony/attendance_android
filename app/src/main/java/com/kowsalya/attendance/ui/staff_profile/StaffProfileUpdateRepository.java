package com.kowsalya.attendance.ui.staff_profile;


import androidx.lifecycle.MutableLiveData;

import com.kowsalya.attendance.retrofit.RetrofitService;
import com.kowsalya.attendance.ui.add_student.ResponseFromAPI;
import com.kowsalya.attendance.ui.student.StudentDetailsBean;
import com.kowsalya.attendance.ui.student.StudentProfileUpdateApi;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class StaffProfileUpdateRepository {

    private static StaffProfileUpdateRepository registerRepository;

    public static StaffProfileUpdateRepository getInstance() {
        if (registerRepository == null) {
            registerRepository = new StaffProfileUpdateRepository();
        }
        return registerRepository;
    }

    private StaffProfileUpdateApi registerApi;

    public StaffProfileUpdateRepository() {
        registerApi = RetrofitService.createService(StaffProfileUpdateApi.class);
    }


    public MutableLiveData<ResponseFromAPI> updateProfile(StaffDetailsBean bean) {
        final MutableLiveData<ResponseFromAPI> mutableLiveData = new MutableLiveData<>();

        registerApi.updateStudent(bean).enqueue(new Callback<ResponseFromAPI>() {
            @Override
            public void onResponse(Call<ResponseFromAPI> call, Response<ResponseFromAPI> response) {
                if (response.isSuccessful()) {

                    mutableLiveData.setValue(response.body());
//                    mutableLiveData.setValue(null);
                }
            }

            @Override
            public void onFailure(Call<ResponseFromAPI> call, Throwable t) {
                mutableLiveData.setValue(null);
            }
        });

        return mutableLiveData;
    }
}
