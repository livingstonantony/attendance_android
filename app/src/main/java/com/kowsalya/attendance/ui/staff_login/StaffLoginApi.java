package com.kowsalya.attendance.ui.staff_login;


import com.kowsalya.attendance.ui.students.StudentsResponse;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Query;

public interface StaffLoginApi {
    @GET("login_staff")
    Call<StaffLoginResponse> getStaffProfile(@Query("id") String id, @Query("password") String password);
}
