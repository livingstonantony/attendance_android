package com.kowsalya.attendance.ui.student_profile;

import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RadioButton;

import com.bosco.attendance.R;
import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;
import com.kowsalya.attendance.interfaces.Config;
import com.kowsalya.attendance.utils.ApplicationSettings;

/**
 * A simple {@link Fragment} subclass.
 */
public class StudentProfileFragment extends Fragment {

    public StudentProfileFragment() {
        // Required empty public constructor
    }

    private ImageView mImageViewProfile;

    private EditText mEditTextId;
    private EditText mEditTextName;
    private EditText mEditTextCourse;

    private RadioButton mRadioButtonMale;
    private RadioButton mRadioButtonFemale;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_student_profile, container, false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        mRadioButtonMale = view.findViewById(R.id.radioButtonMale);
        mRadioButtonFemale = view.findViewById(R.id.radioButtonFemale);

        mImageViewProfile = view.findViewById(R.id.imageViewProfile);

        mEditTextId = view.findViewById(R.id.editTextId);
        mEditTextName = view.findViewById(R.id.editTextName);
        mEditTextCourse = view.findViewById(R.id.editTextCourse);


        Glide.with(getContext()).load(ApplicationSettings.read(Config.STUDENT_PROFILE, "")).apply(new RequestOptions().placeholder(R.drawable.person)).into(mImageViewProfile);

        mEditTextId.setText(ApplicationSettings.read(Config.STUDENT_ID, ""));
        mEditTextName.setText(ApplicationSettings.read(Config.STUDENT_NAME, ""));
        mEditTextCourse.setText(ApplicationSettings.read(Config.STUDENT_COURSE, ""));

        int gender = ApplicationSettings.read(Config.STUDENT_GENDER, 0);

        if (gender == 0) {
            mRadioButtonMale.setChecked(true);
            mRadioButtonFemale.setChecked(false);
        } else {
            mRadioButtonMale.setChecked(false);
            mRadioButtonFemale.setChecked(true);
        }
    }
}
