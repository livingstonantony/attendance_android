package com.kowsalya.attendance.ui.add_student;


import com.kowsalya.attendance.ui.student_login.StudentLoginResponse;

import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.GET;
import retrofit2.http.Headers;
import retrofit2.http.POST;
import retrofit2.http.Query;

public interface AddStudentApi {
    @Headers( "Content-Type: application/json" )
    @POST("register_student")
    Call<ResponseFromAPI> registerStudent(@Body AddStudentPost registerPost);


    @GET("student")
    Call<StudentLoginResponse> getStudent(@Query("id") String id);

}
