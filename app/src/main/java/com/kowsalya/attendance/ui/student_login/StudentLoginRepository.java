package com.kowsalya.attendance.ui.student_login;


import androidx.lifecycle.MutableLiveData;

import com.kowsalya.attendance.retrofit.RetrofitService;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class StudentLoginRepository {

    private static StudentLoginRepository registerRepository;

    public static StudentLoginRepository getInstance() {
        if (registerRepository == null) {
            registerRepository = new StudentLoginRepository();
        }
        return registerRepository;
    }

    private StudentLoginApi registerApi;

    public StudentLoginRepository() {
        registerApi = RetrofitService.createService(StudentLoginApi.class);
    }


    public MutableLiveData<StudentLoginResponse> getStudentProfile(String id, String password) {
        final MutableLiveData<StudentLoginResponse> mutableLiveData = new MutableLiveData<>();

        registerApi.getStudentProfile(id, password).enqueue(new Callback<StudentLoginResponse>() {
            @Override
            public void onResponse(Call<StudentLoginResponse> call, Response<StudentLoginResponse> response) {
                if (response.isSuccessful()) {
                    mutableLiveData.setValue(response.body());
                }
            }

            @Override
            public void onFailure(Call<StudentLoginResponse> call, Throwable t) {
                mutableLiveData.setValue(null);
            }
        });

        return mutableLiveData;
    }
}
