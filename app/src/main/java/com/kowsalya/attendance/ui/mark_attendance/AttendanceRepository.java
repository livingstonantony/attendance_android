package com.kowsalya.attendance.ui.mark_attendance;


import androidx.lifecycle.MutableLiveData;

import com.kowsalya.attendance.retrofit.RetrofitService;
import com.kowsalya.attendance.ui.add_student.ResponseFromAPI;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class AttendanceRepository {

    private static AttendanceRepository registerRepository;

    public static AttendanceRepository getInstance() {
        if (registerRepository == null) {
            registerRepository = new AttendanceRepository();
        }
        return registerRepository;
    }

    private AttendanceApi registerApi;

    public AttendanceRepository() {
        registerApi = RetrofitService.createService(AttendanceApi.class);
    }


    public MutableLiveData<ResponseFromAPI> setAttendance(AttendanceList list) {
        final MutableLiveData<ResponseFromAPI> mutableLiveData = new MutableLiveData<>();

        registerApi.setAttendance(list).enqueue(new Callback<ResponseFromAPI>() {
            @Override
            public void onResponse(Call<ResponseFromAPI> call, Response<ResponseFromAPI> response) {
                if (response.isSuccessful()) {

                    mutableLiveData.setValue(response.body());
//                    mutableLiveData.setValue(null);
                }
            }

            @Override
            public void onFailure(Call<ResponseFromAPI> call, Throwable t) {
                mutableLiveData.setValue(null);
            }
        });

        return mutableLiveData;
    }
}
