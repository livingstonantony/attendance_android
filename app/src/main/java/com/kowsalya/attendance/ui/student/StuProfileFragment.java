package com.kowsalya.attendance.ui.student;

import android.Manifest;
import android.content.Intent;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AlertDialog;
import androidx.core.content.FileProvider;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProvider;

import android.os.Environment;
import android.provider.MediaStore;
import android.provider.Settings;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RadioButton;
import android.widget.Toast;

import com.bosco.attendance.R;
import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;
import com.google.android.material.button.MaterialButton;
import com.karumi.dexter.Dexter;
import com.karumi.dexter.MultiplePermissionsReport;
import com.karumi.dexter.PermissionToken;
import com.karumi.dexter.listener.PermissionRequest;
import com.karumi.dexter.listener.multi.MultiplePermissionsListener;
import com.kowsalya.attendance.interfaces.Config;
import com.kowsalya.attendance.ui.add_student.AddStudentPost;
import com.kowsalya.attendance.ui.add_student.AddStudentViewModel;
import com.kowsalya.attendance.ui.add_student.ResponseFromAPI;
import com.kowsalya.attendance.utils.ApplicationSettings;
import com.kowsalya.attendance.utils.EncodeBased64Binary;
import com.kowsalya.attendance.utils.FileCompressor;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import static android.app.Activity.RESULT_OK;

/**
 * A simple {@link Fragment} subclass.
 */
public class StuProfileFragment extends Fragment {


    private ImageView mImageViewProfile;

    private EditText mEditTextId;
    private EditText mEditTextName;
    private EditText mEditTextCourse;

    private RadioButton mRadioButtonMale;
    private RadioButton mRadioButtonFemale;

    private StudentUpdateProfileViewModel viewModel;

    private Button mButtonUpdate;


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_stu_profile, container, false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        viewModel = new ViewModelProvider(this).get(StudentUpdateProfileViewModel.class);

        mRadioButtonMale = view.findViewById(R.id.radioButtonMale);
        mRadioButtonFemale = view.findViewById(R.id.radioButtonFemale);

        mImageViewProfile = view.findViewById(R.id.imageViewProfile);

        mEditTextId = view.findViewById(R.id.editTextId);
        mEditTextName = view.findViewById(R.id.editTextName);
        mEditTextCourse = view.findViewById(R.id.editTextCourse);

        mButtonUpdate = view.findViewById(R.id.buttonUpdate);


        Glide.with(getContext()).load(ApplicationSettings.read(Config.STUDENT_PROFILE, "")).apply(new RequestOptions().placeholder(R.drawable.person)).into(mImageViewProfile);

        mEditTextId.setText(ApplicationSettings.read(Config.STUDENT_ID, ""));
        mEditTextName.setText(ApplicationSettings.read(Config.STUDENT_NAME, ""));
        mEditTextCourse.setText(ApplicationSettings.read(Config.STUDENT_COURSE, ""));

        int gender = ApplicationSettings.read(Config.STUDENT_GENDER, 0);

        if (gender == 0) {
            mRadioButtonMale.setChecked(true);
            mRadioButtonFemale.setChecked(false);
        } else {
            mRadioButtonMale.setChecked(false);
            mRadioButtonFemale.setChecked(true);
        }


        mButtonUpdate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                String id = mEditTextId.getText().toString().trim();
                String name = mEditTextName.getText().toString().trim();
                String course = mEditTextCourse.getText().toString();
                int gender = 0;

                if (mRadioButtonMale.isChecked()) {
                    gender = 0;
                } else {
                    gender = 1;
                }


                StudentDetailsBean bean = new StudentDetailsBean( id, name, course, gender);

                viewModel.init(bean);
                viewModel.getRegisterResponse().observe(getViewLifecycleOwner(), new Observer<ResponseFromAPI>() {
                    @Override
                    public void onChanged(ResponseFromAPI registerResponse) {
                        Toast.makeText(getContext(), "" + registerResponse, Toast.LENGTH_SHORT).show();
                    }
                });

            }
        });
    }


}
