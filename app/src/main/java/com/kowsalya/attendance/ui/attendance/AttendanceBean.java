package com.kowsalya.attendance.ui.attendance;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class AttendanceBean {

    @SerializedName("response")
    @Expose
    private List<AttendanceData> list;


    public AttendanceBean(List<AttendanceData> list) {
        this.list = list;
    }

    public List<AttendanceData> getList() {
        return list;
    }

    public void setList(List<AttendanceData> list) {
        this.list = list;
    }

    public static class AttendanceData {

        @SerializedName("url")
        @Expose
        private String url;

        @SerializedName("name")
        @Expose
        private String name;


        public AttendanceData(String url, String name) {
            this.url = url;
            this.name = name;
        }

        public String getUrl() {
            return url;
        }

        public void setUrl(String url) {
            this.url = url;
        }

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }
    }
}
