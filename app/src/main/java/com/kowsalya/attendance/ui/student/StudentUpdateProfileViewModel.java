package com.kowsalya.attendance.ui.student;

import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.ViewModel;

import com.kowsalya.attendance.ui.add_student.AddStudentPost;
import com.kowsalya.attendance.ui.add_student.AddStudentRepository;
import com.kowsalya.attendance.ui.add_student.ResponseFromAPI;

public class StudentUpdateProfileViewModel extends ViewModel {

    private MutableLiveData<String> mText;
    private MutableLiveData<ResponseFromAPI> mutableLiveData;
    private StudentProfileUpdateRepository repository;

    public void init(StudentDetailsBean bean) {
       /* if (mutableLiveData != null){
            return;
        }*/
        repository = StudentProfileUpdateRepository.getInstance();

        mutableLiveData = repository.updateProfile(bean);

    }

    public StudentUpdateProfileViewModel() {
        mText = new MutableLiveData<>();
        mText.setValue("This is attendance fragment");
    }


    public LiveData<ResponseFromAPI> getRegisterResponse() {
        return mutableLiveData;
    }
}