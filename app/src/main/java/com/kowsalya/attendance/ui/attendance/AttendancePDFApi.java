package com.kowsalya.attendance.ui.attendance;


import com.kowsalya.attendance.ui.students.StudentsResponse;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Query;

public interface AttendancePDFApi {
    @GET("get_all_attendance")
    Call<AttendanceBean> getAllPDF(@Query("id") String id
            , @Query("name") String name
            , @Query("subject") String subject);

    @GET("get_single_attendance")
    Call<AttendanceBean> getSinglePDF(@Query("id") String id);
}
