package com.kowsalya.attendance;

import android.app.Activity;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;

import com.bosco.attendance.R;
import com.google.android.material.navigation.NavigationView;
import com.kowsalya.attendance.interfaces.Config;
import com.kowsalya.attendance.ui.activities.ExportAllStudentPDFActivity;
import com.kowsalya.attendance.ui.add_student.ResponseFromAPI;
import com.kowsalya.attendance.ui.satff_change_password.ChangePasswordBean;
import com.kowsalya.attendance.ui.satff_change_password.StaffChangePasswordViewModel;
import com.kowsalya.attendance.utils.ApplicationSettings;
import com.kowsalya.attendance.utils.Utils;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AlertDialog;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProvider;
import androidx.navigation.NavController;
import androidx.navigation.NavDestination;
import androidx.navigation.Navigation;
import androidx.navigation.ui.AppBarConfiguration;
import androidx.navigation.ui.NavigationUI;
import androidx.drawerlayout.widget.DrawerLayout;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;

public class MainActivity extends AppCompatActivity {

    private AppBarConfiguration mAppBarConfiguration;

    private MenuItem mItemChangePassword;
    private MenuItem mItemAllPDF;
    private MenuItem mItemSinglePDF;
    private MenuItem mItemSettings;

    private int id;

    private StaffChangePasswordViewModel viewModel;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        viewModel = new ViewModelProvider(this).get(StaffChangePasswordViewModel.class);


        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        DrawerLayout drawer = findViewById(R.id.drawer_layout);
        NavigationView navigationView = findViewById(R.id.nav_view);
        // Passing each menu ID as a set of Ids because each
        // menu should be considered as top level destinations.
        mAppBarConfiguration = new AppBarConfiguration.Builder(
                R.id.nav_add_student, R.id.nav_profile, R.id.nav_mark_attendance, R.id.nav_student, R.id.nav_logout)
                .setDrawerLayout(drawer)
                .build();
        NavController navController = Navigation.findNavController(this, R.id.nav_host_fragment);
        NavigationUI.setupActionBarWithNavController(this, navController, mAppBarConfiguration);
        NavigationUI.setupWithNavController(navigationView, navController);

        navController.addOnDestinationChangedListener(new NavController.OnDestinationChangedListener() {
            @Override
            public void onDestinationChanged(@NonNull NavController controller, @NonNull NavDestination destination, @Nullable Bundle arguments) {

                id = destination.getId();

                Log.d("Fragment", "onDestinationChanged: " + destination.getLabel() + " : " + destination.getId());
            }
        });
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.main, menu);

        return true;
    }

    @Override
    public boolean onPrepareOptionsMenu(Menu menu) {
        mItemChangePassword = menu.findItem(R.id.menu_staff_change_password);
        mItemAllPDF = menu.findItem(R.id.menu_all_pdf);
        mItemSinglePDF = menu.findItem(R.id.menu_single_PDF);
        mItemSettings = menu.findItem(R.id.menu_settings);

        if (mItemChangePassword != null) {
            mItemChangePassword.setVisible(false);
            mItemAllPDF.setVisible(false);
            mItemSinglePDF.setVisible(false);
//            mItemSettings.setVisible(false);

            switch (id) {
                case R.id.nav_profile:
                    mItemChangePassword.setVisible(true);
                    break;
                case R.id.nav_student:
                    mItemAllPDF.setVisible(true);
                    break;
                case R.id.studentProfileFragment:
                    mItemSinglePDF.setVisible(true);
                    break;
            }
        }
        return true;
    }

    @Override
    public boolean onSupportNavigateUp() {
        hideSoftKeyboard(this);
        NavController navController = Navigation.findNavController(this, R.id.nav_host_fragment);
        return NavigationUI.navigateUp(navController, mAppBarConfiguration)
                || super.onSupportNavigateUp();
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        switch (item.getItemId()) {
            case R.id.menu_staff_change_password:
                getPassword();
                break;
            case R.id.menu_all_pdf:
                Intent intent = new Intent(this, ExportAllStudentPDFActivity.class);
                startActivity(intent);
                break;
        }

        return super.onOptionsItemSelected(item);
    }

    private void getPassword() {
        AlertDialog dialogBox = Utils.showDialog(this);
        AlertDialog.Builder alert = new AlertDialog.Builder(this);
        final EditText edittext = new EditText(this);
        alert.setMessage("Password");
        alert.setTitle("Change your password");

        alert.setView(edittext);

        alert.setPositiveButton("CHANGE", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int whichButton) {
                //What ever you want to do with the value
                //OR
                String pwd = edittext.getText().toString();

                dialog.dismiss();

                dialogBox.show();

                Log.d("ID ", " " + ApplicationSettings.read(Config.ID, "") + " : " + pwd);

                viewModel.init(new ChangePasswordBean(ApplicationSettings.read(Config.ID, ""), pwd));

                viewModel.getResponse().observe(MainActivity.this, new Observer<ResponseFromAPI>() {
                    @Override
                    public void onChanged(ResponseFromAPI responseFromAPI) {
                        dialogBox.dismiss();

                        if (responseFromAPI != null) {
                            Utils.showMSG(MainActivity.this, "Pssword", "Password changed successfully");
                        } else {
                            Utils.showMSG(MainActivity.this, "Pssword", "There was some problem, Try agian later");

                        }
                    }
                });

            }
        });

        alert.setNegativeButton("CANCEL", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int whichButton) {
                // what ever you want to do with No option.
            }
        });

        alert.show();
    }

    public void hideSoftKeyboard(Activity activity) {

        // Check if no view has focus:
        View view = activity.getCurrentFocus();
        if (view != null) {
            InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
            imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
        }
    }
}
